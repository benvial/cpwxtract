#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of cpwxtract
# License: GPLv3
# See the documentation at benvial.gitlab.io/cpwxtract


import jax.numpy as np
from jax import jit
from scipy.special import ellipk

pi = np.pi

deg2rad = pi / 180
rad2deg = 1 / deg2rad


def complementary(k):
    return (1 - k ** 2) ** 0.5


def unwrap(phi):
    return np.unwrap(phi * deg2rad) * rad2deg


def meas2complex(A, phi):
    return 10 ** (A / 20) * np.exp(1j * phi * deg2rad)


def complex2aphi(z):
    return np.abs(z), np.unwrap(np.angle(z)) * rad2deg


def K_scipy(k):
    return ellipk(k)


def Kp_scipy(k):
    return K_scipy(complementary(k))


def kokp_scipy(k):
    return K_scipy(k) / Kp_scipy(k)


def _K(k):
    t = np.linspace(0, pi / 2, 1000)
    if not np.isscalar(k):
        t, k = np.meshgrid(t, k)
    return np.trapz((1 - k * np.sin(t) ** 2) ** (-0.5), t)


@jit
def K(k):
    return _K(k)
    # return np.where(np.abs(k) < 1-1e-3 ** 0.5, _K(k), _K(1/k)/(k)**0.5)


def Kp(k):
    return K(complementary(k))


def kokp(k):
    return K(k) / Kp(k)


def kokp_approx(k):
    kp = complementary(k)
    val0 = pi / np.log(2 * (1 + kp ** 0.5) / (1 - kp ** 0.5))
    val1 = np.log(2 * (1 + k ** 0.5) / (1 - k ** 0.5)) / pi
    return np.where(np.abs(k) < 1 / 2 ** 0.5, val0, val1)


def S2ABCD(S, Z0=50):
    DeltaS = S["S11"] * S["S22"] - S["S21"] * S["S12"]
    A = (1 + S["S11"] - S["S22"] - DeltaS) / (2 * S["S21"])
    B = (1 + S["S11"] + S["S22"] + DeltaS) * Z0 / (2 * S["S21"])
    C = (1 - S["S11"] - S["S22"] + DeltaS) / (2 * S["S21"] * Z0)
    D = (1 - S["S11"] + S["S22"] - DeltaS) / (2 * S["S21"])
    return dict(A=A, B=B, C=C, D=D)


#
# def kokp(k):
#     return kokp_approx(k)
#
# import matplotlib.pyplot as plt
#
# plt.ion()
# plt.close("all")
# k = np.linspace(0.0, 1, 1000)
# # plt.figure()
# # plt.plot(k, K_scipy(k))
# # plt.plot(k, K(k), "o")
#
#
# plt.figure()
# plt.plot(k, kokp(k))
# plt.plot(k, kokp_scipy(k), "sk")
# plt.plot(k, kokp_approx(k), "o")
#
# # plt.figure()
# # plt.plot(k,np.abs(kokp(k)-kokp_scipy(k)))
# # plt.yscale("log")
# #
# # plt.figure()
# # plt.plot(k,np.abs(kokp_approx(k)-kokp_scipy(k)))
# # plt.yscale("log")
#
#
# plt.figure()
# plt.plot(k,np.abs(K(k)-K_scipy(k)))
# plt.yscale("log")
