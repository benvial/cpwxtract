#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of cpwxtract
# License: GPLv3
# See the documentation at benvial.gitlab.io/cpwxtract


"""
Defines physical and mathematical constants:

- ``c``: speed of light in vaccum
- ``epsilon_0``: vaccum permittivity
- ``mu_0``: vaccum permeability
- ``Z_0``: vaccum impedance
- ``pi``: ratio of a circle's circumference to its diameter
    
"""

from scipy.constants import c, epsilon_0, mu_0, pi

Z_0 = (mu_0 / epsilon_0) ** 0.5
