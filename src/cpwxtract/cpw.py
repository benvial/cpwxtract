#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of cpwxtract
# License: GPLv3
# See the documentation at benvial.gitlab.io/cpwxtract

"""
Model from :cite:p:`Heinrich1993`.
"""


__all__ = ["CPW"]

import copy

import jax.numpy as np

from .constants import *
from .helpers import K, Kp, kokp


class CPW:
    def __init__(self, L, w, s, h=0, thickness=None, epsilon=1, grounded=False, rho=0):
        self.L = L * 1e-3
        self.w = w * 1e-3
        self.h = h * 1e-3
        self.s = s * 1e-3
        self.thickness = thickness if thickness is None else thickness * 1e-3
        self.epsilon = epsilon
        self.grounded = grounded
        self.rho = rho

        if grounded and thickness is None:
            raise ValueError("thickness must be finite when grounded is True")

        if rho > 0 and h == 0:
            raise ValueError("h must be >0 when rho >0")

        self.Delta = (
            1.25 * self.h / pi * (1 + np.log(4 * pi * self.w / self.h))
            if self.h > 0
            else 0
        )
        self.s = self.s - self.Delta
        self.w = self.w + self.Delta

        self.k1 = self.w / (self.w + 2 * self.s)
        # if self.h == 0:
        #     self.k1 = self.w / (self.w + 2 * self.s)
        # else:
        #     self.k1 = self.we / (self.we + 2 * self.se)

        self.k2 = (
            0
            if self.thickness is None
            else np.sinh(pi * self.w / (4 * self.thickness))
            / np.sinh(pi * (self.w + 2 * self.s) / (4 * self.thickness))
        )
        self.k3 = (
            0
            if self.thickness is None
            else np.tanh(pi * self.w / (4 * self.thickness))
            / np.tanh(pi * (self.w + 2 * self.s) / (4 * self.thickness))
        )

    @property
    def effective_epsilon(self):
        if self.thickness is None:
            eps_eff = (self.epsilon + 1) / 2
        else:
            if self.grounded:
                q = kokp(self.k3) / (kokp(self.k1) + kokp(self.k3))
                eps_eff = 1 + q * (self.epsilon - 1)
            else:
                eps_eff = 1 + (self.epsilon - 1) / 2 * kokp(self.k2) / kokp(self.k1)
        if self.h > 0:
            eps_eff = eps_eff - 0.7 * (eps_eff - 1) * self.h / self.s / (
                kokp(self.k1) + 0.7 * self.h / self.s
            )
        return eps_eff

    @property
    def impedance(self):
        if self.grounded:
            return (
                60
                * pi
                / (self.effective_epsilon) ** 0.5
                / (kokp(self.k1) + kokp(self.k3))
            )
        else:
            return 30 * pi / (self.effective_epsilon) ** 0.5 / kokp(self.k1)

    def propagation_loss(self, frequency):
        if self.rho == 0:
            return 0
        else:
            omega = frequency * 2 * pi * 1e9
            mu = 1
            # skin depth
            delta = (2 * self.rho / (omega * mu * mu_0)) ** 0.5
            Rs = self.rho / delta
            a = self.w / 2
            b = self.s + self.w / 2
            k1 = self.k1
            t = self.h
            alpha = (
                Rs
                * (self.effective_epsilon) ** 0.5
                / (480 * pi * K(k1) * Kp(k1) * (1 - k1 ** 2))
            )
            tmp = (pi + np.log(8 * pi * a * (1 - k1) / (t * (1 + k1)))) / a
            tmp += (pi + np.log(8 * pi * b * (1 - k1) / (t * (1 + k1)))) / b
            alpha *= tmp

            alpha = 20 * np.log10(alpha)

            return alpha

    def get_S(self, frequency, impedance=None):
        omega = frequency * 2 * pi * 1e9
        k0 = omega / c
        Z0 = 50  # self.cpw0.impedance
        if impedance is None:
            Zc = self.impedance
        else:
            Zc = impedance
        # Zc = Z0 * np.real(1 + S11_target) / (1 - S11_target)
        gamma = 1j * (self.effective_epsilon) ** 0.5 * k0 + 1j * self.propagation_loss(
            frequency
        )
        # gamma = np.where(gamma.imag<0,-gamma,gamma)
        sh = np.sinh(gamma * self.L)
        ch = np.cosh(gamma * self.L)
        denom = 2 * Zc * Z0 * ch + (Zc ** 2 + Z0 ** 2) * sh
        S11 = (Zc ** 2 - Z0 ** 2) * sh / denom
        S12 = 2 * Zc * Z0 / denom
        S21 = S12
        S22 = S11
        return dict(S11=S11, S12=S12, S21=S21, S22=S22)
