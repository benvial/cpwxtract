

[![](https://img.shields.io/endpoint?url=https://gitlab.com/benvial/cpwxtract/-/jobs/artifacts/main/file/logobadge.json?job=badge)](https://gitlab.com/benvial/cpwxtract/-/releases)
[![](https://img.shields.io/gitlab/pipeline-status/benvial/cpwxtract?branch=main&logo=gitlab&labelColor=dddddd&logoColor=ffffff&style=for-the-badge)](https://gitlab.com/benvial/cpwxtract/commits/main)
[![](https://img.shields.io/gitlab/coverage/benvial/cpwxtract/main?logo=python&logoColor=e9d672&style=for-the-badge)](https://benvial.gitlab.io/cpwxtract/coverage)
[![](https://img.shields.io/badge/code%20style-black-dedede.svg?logo=python&logoColor=e9d672&style=for-the-badge)](https://black.readthedocs.io/en/stable/)
[![](https://img.shields.io/badge/license-GPLv3-blue?color=439cb0&logo=open-access&logoColor=white&style=for-the-badge)](https://gitlab.com/benvial/cpwxtract/-/blob/main/LICENSE.txt)



# cpwxtract

Tools for extracting material properties from coplanar waveguide measurements.

See the documentation at [benvial.gitlab.io/cpwxtract](https://benvial.gitlab.io/cpwxtract).
