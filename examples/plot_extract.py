#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of cpwxtract
# License: GPLv3
# See the documentation at benvial.gitlab.io/cpwxtract


"""
Basic extraction
================

Tutorial.
"""

import matplotlib.pyplot as plt
import numpy as np

import cpwxtract as cp

plt.style.use("../doc/cpwxtract.mplstyle")

###############################################################################
# Load data


sample_file = f"./data/alumina-uncoated-5mm.s2p"

frequencies = np.arange(1.5, 5, 0.025)
Smeas = cp.get_Smeas(sample_file, frequencies)


###############################################################################
# Define CPW

L = 5  # Length of transmission line in mm
w = 0.347  # width of central conductor in mm
s = 0.175  # gap from centre to ground in mm
h_metal = 1e-3

grounded = False
thickness = None

cpw = cp.CPW(L, w, s, h_metal, thickness=thickness, epsilon=1, grounded=grounded)


###############################################################################
# Extract

guess = 11 * (1 - 0 * 1j)

ext = cp.Extractor(
    frequencies,
    Smeas,
    guess,
    cpw,
    eps_min=1 - 20 * 1j,
    eps_max=20 + 0 * 1j,
    options=None,
    verbose=False,
)
ext.run()

eps_opt = ext.eps_opt
print(f">>> objective = {ext.opt.fun}")


###############################################################################
# PLot permittivity


cols = plt.rcParams["axes.prop_cycle"].by_key()["color"]

plt.close("all")
plt.figure()
plt.plot(frequencies, eps_opt.real, label="Re", c=cols[0])
plt.plot(frequencies, -eps_opt.imag, "--", label="Im", c=cols[1])
plt.xlabel("frequency (GHz)")
plt.ylabel("permittivity")
plt.legend()
plt.tight_layout()


###############################################################################
# PLot S parameters


Sopt = ext.cpw.get_S(frequencies)

fig, ax = plt.subplots(2, 2, figsize=(3.5, 4.5))
ax[0][0].plot(frequencies, np.array(Smeas["S11"]).real, label="Re meas", c=cols[0])
ax[0][0].plot(frequencies, np.array(Sopt["S11"]).real, "--", label="Re fit", c=cols[0])
ax[0][0].plot(frequencies, np.array(Smeas["S11"]).imag, label="Im meas", c=cols[1])
ax[0][0].plot(frequencies, np.array(Sopt["S11"]).imag, "--", label="Im fit", c=cols[1])
ax[1][0].plot(frequencies, np.array(Smeas["S21"]).real, label="Re meas", c=cols[0])
ax[1][0].plot(frequencies, np.array(Sopt["S21"]).real, "--", label="Re fit", c=cols[0])
ax[1][0].plot(frequencies, np.array(Smeas["S21"]).imag, label="Im meas", c=cols[1])
ax[1][0].plot(frequencies, np.array(Sopt["S21"]).imag, "--", label="Im fit", c=cols[1])
ax[0][1].plot(frequencies, np.array(Smeas["S12"]).real, label="Re meas", c=cols[0])
ax[0][1].plot(frequencies, np.array(Sopt["S12"]).real, "--", label="Re fit", c=cols[0])
ax[0][1].plot(frequencies, np.array(Smeas["S12"]).imag, label="Im meas", c=cols[1])
ax[0][1].plot(frequencies, np.array(Sopt["S12"]).imag, "--", label="Im fit", c=cols[1])
ax[1][1].plot(frequencies, np.array(Smeas["S22"]).real, label="Re meas", c=cols[0])
ax[1][1].plot(frequencies, np.array(Sopt["S22"]).real, "--", label="Re fit", c=cols[0])
ax[1][1].plot(frequencies, np.array(Smeas["S22"]).imag, label="Im meas", c=cols[1])
ax[1][1].plot(frequencies, np.array(Sopt["S22"]).imag, "--", label="Im fit", c=cols[1])
ax[0][0].set_title("$S_{11}$")
ax[0][1].set_title("$S_{12}$")
ax[1][0].set_title("$S_{21}$")
ax[1][1].set_title("$S_{22}$")
for a in ax.ravel():
    a.set_xlabel("frequency (GHz)")
    a.set_ylim(-1, 1)
    a.legend()
plt.tight_layout()
