.. cpwxtract documentation master file

=========
cpwxtract
=========

**Tools for extracting material properties from coplanar waveguide measurements.** 


Welcome! This is the documentation for cpwxtract |release|, last updated on |today|.



.. toctree::
   :maxdepth: 2
   :caption: Quickstart
   
   quickstart
   
   
.. toctree::
   :maxdepth: 2
   :caption: Examples

   auto_examples/index

   
   
.. toctree::
  :maxdepth: 2
  :caption: API reference:
  :hidden:
  
  source/modules
  idx


  
.. toctree::
 :maxdepth: 2
 :caption: Bibliography:
 :hidden:
 
 biblio
  
